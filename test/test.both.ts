import { expect, tap } from '@push.rocks/tapbundle';
import * as deesComms from '../ts/index.js';

let deesCommsTest: deesComms.DeesComms;
let deesCommsTest2: deesComms.DeesComms;

tap.test('first test', async (tools) => {
  deesCommsTest = new deesComms.DeesComms();
  deesCommsTest2 = new deesComms.DeesComms();
  let counter = 1;
  deesCommsTest2.createTypedHandler<any>('test', async (requestData) => {
    console.log(`got the request ${counter++}`);
    return { hitheretoo: `greetings to ${requestData.hithere}` };
  });

  // lets fire a request
  const typedrequest = deesCommsTest.createTypedRequest<any>('test');
  const result = await typedrequest.fire({
    hithere: 'hello',
  });
  console.log(JSON.stringify(result));

  // lets fire a request
  const typedrequest2 = deesCommsTest.createTypedRequest<any>('test2');
  // TODO: return response after timeout
  /* const result2 = await typedrequest2.fire({
    hithere: 'hello',
  });
  console.log(JSON.stringify(result2)); */
});

tap.test('should end on nodejs', async (toolsArg) => {
  if (globalThis.process) {
    toolsArg.delayFor(2000).then(() => globalThis.process.exit(0));
  }
})

tap.start();
