import * as plugins from './dees-comms.plugins.js';

/**
 * a message that can be sent
 */
export class DeesCommsMessage {}
