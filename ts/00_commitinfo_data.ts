/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@design.estate/dees-comms',
  version: '1.0.24',
  description: 'a comms module for communicating within the DOM'
}
